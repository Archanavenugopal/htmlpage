package profile_demo.springprofiles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "github")
@Data
@ToString
@NoArgsConstructor
public class GitHubUser {
    @Id
    private Long id;
    private String login;
    private String avatar_url;
    private String name;
    private Long followers;
}

