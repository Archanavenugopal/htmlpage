package profile_demo.springprofiles.Service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import profile_demo.springprofiles.domain.GitHubUser;
import profile_demo.springprofiles.repo.CustomerRepo;

import java.util.List;

@Service
public class GitHubService {
    private RestTemplate restTemplate;
    private CustomerRepo customerRepo;

    public GitHubService(RestTemplate restTemplate, CustomerRepo customerRepo) {
        this.restTemplate = restTemplate;
        this.customerRepo = customerRepo;
    }

    public GitHubUser save(GitHubUser gitHubUser){
        return this.customerRepo.save(gitHubUser);
    }
    public List<GitHubUser> listUser(){
        System.out.println("Inside Service");
        return  this.customerRepo.findAll();
    }

    }


