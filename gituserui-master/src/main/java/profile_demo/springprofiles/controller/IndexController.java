package profile_demo.springprofiles.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import profile_demo.springprofiles.Service.GitHubService;
import profile_demo.springprofiles.domain.GitHubUser;

import java.util.List;

@Controller
public class IndexController {

    private RestTemplate template;

    private GitHubService gitHubService;

    public IndexController(RestTemplate template, GitHubService gitHubService, JdbcTemplate jdbcTemplate) {
        this.template = template;
        this.gitHubService = gitHubService;
        this.jdbcTemplate = jdbcTemplate;
    }

    private JdbcTemplate jdbcTemplate;



    @GetMapping("/")
    public String index(@RequestParam(name = "username", defaultValue = "atinsingh") String username, Model model){
        System.out.println("******** GOT HERE *** with PARAM "+ username);
        GitHubUser user = template.getForObject("https://api.github.com/users/{username}",
                GitHubUser.class,
                new Object[]{username});
        model.addAttribute("user", user);
        gitHubService.save(user);
        return "index";
    }

    @GetMapping("/listUser")
    public String listUser(Model model){
        System.out.println("Inside Controller");
        System.out.println(gitHubService.listUser());
        List<GitHubUser> user=gitHubService.listUser();
        model.addAttribute("abc",user);
        return "table";

        //String sql = "SELECT * FROM github";
        //jdbcTemplate.query(sql,BeanPropertyRowMapper<GitHubUser.class>);


    }


}
